//
//  AddAccountRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 5.0

func AddAccountRequest(token:String, accountData:AddAccountRequestModel, completion:@escaping(AccountModel?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
    var configuration = Configuration()

    let url = "\(configuration.enviroment.base_url)/account/create"
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(url, method: .post, parameters: accountData, encoder: JSONParameterEncoder.default, headers: headders){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseObjectModel<AccountModel>.self) { response in
        switch response.result {
            case .success:
                if let data = response.value {
                    completion(data.data)
                }
            case let .failure(error):
            
                _ = response.response?.statusCode
            completionError(ErrorModel(message: error.localizedDescription, statusCode: error.responseCode ?? 0))
        }
    }
}
