//
//  TransactionsRequests.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import Foundation
import Alamofire

func getListTransactionsRequest(token:String, accountId:String, completion: @escaping ([TransactionModelSort]?) -> Void, completionError: @escaping (ErrorModel?) -> Void){
    var configuration = Configuration()

    let URL = configuration.enviroment.base_url + "/registers/byAccount/\(accountId)"
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(URL,
               method: .get,
               encoding: JSONEncoding.default,
               headers: headders)
    .responseDecodable(of: BaseArrayModel<TransactionModelSort>.self) { response in
        switch response.result {
           case .success:
            
            guard let data = response.value, data.respuesta else{
                completionError(ErrorModel(message: "Error: \(response.error!)", statusCode: response.response?.statusCode ?? 0))
                return
            }
            completion(data.data)
           case .failure(let error):
            completionError(ErrorModel(message: "Error de repsueta: \(error)", statusCode: error.responseCode ?? 0))
        }
    }
}
