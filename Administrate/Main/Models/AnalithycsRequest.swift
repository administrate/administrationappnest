//
//  AnalithycsRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/06/22.
//

import Foundation
import Alamofire

func AnalithycsRequest(token:String, idAccount:String, completion:@escaping(_ response:[ResumeModel]?) -> Void, failure:@escaping(_ response:ErrorModel?) -> Void){
    let timeoutInterval: Double = 5.0
    var configuration = Configuration()
    let url = "\(configuration.enviroment.base_url)/registers/graphs/\(idAccount)"
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(url, method: .get, headers: headders){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseArrayModel<ResumeModel>.self){ response in
        switch response.result {
            case .success:
                if let data = response.value {
                    completion(data.data)
                }
            case let .failure(error):
                _ = response.response?.statusCode
            failure(ErrorModel(message: error.localizedDescription, statusCode: error.responseCode ?? 0))
        }
    }
}
