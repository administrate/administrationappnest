//
//  StatusManager.swift
//  Administrate
//
//  Created by Cristian Canedo on 09/06/22.
//
import Foundation
import CoreData
import UIKit

public class StatusManagerLDB{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: StatusManagerLDB = {
            let instance = StatusManagerLDB()
            return instance
    }()
    
    private func createModel(model:StatusModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "StatusLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(id: model._id){
            return updateModel(entity:entity, model: model)
        }else{
            let res = StatusLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.createAt = model.createAt
            res.data = model.data
            res.desc = model.description
            res.updateAt = model.updateAt
            res.version = NSNumber(value: model.__v)
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[StatusModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:StatusLDBModel, model:StatusModel) -> Bool{
        entity.createAt = model.createAt
        entity.data = model.data
        entity.desc = model.description
        entity.updateAt = model.updateAt
        entity.version = NSNumber(value: model.__v)
        
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels() -> [StatusModel]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "StatusLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[StatusModel] = []
            for item in fetching {
                let it = item as! StatusLDBModel
                let id = it.id != nil ? it.id!:""
                let description = it.desc != nil ? it.desc!:""
                let data = it.data != nil ? it.data!:""
                let created = it.createAt != nil ? it.createAt!:""
                let updated = it.updateAt != nil ? it.updateAt!:""
                let ver = Double(exactly: it.version ?? 0.0)!
                
                arrayResult.append(StatusModel(_id: id, description: description, data: data, createAt: created, updateAt: updated, __v: ver))
            }
            return arrayResult
        }catch{
            return []
        }
    }
    
    func getModelsbyId(id:String) ->StatusModel?{
        guard let it = ifExist(id: id) else {return nil}
        let id = it.id != nil ? it.id!:""
        let description = it.desc != nil ? it.desc!:""
        let data = it.data != nil ? it.data!:""
        let created = it.createAt != nil ? it.createAt!:""
        let updated = it.updateAt != nil ? it.updateAt!:""
        let ver = Double(exactly: it.version ?? 0.0)!
        return StatusModel(_id: id, description: description, data: data, createAt: created, updateAt: updated, __v: ver)
    }
    
    private func ifExist(id:String) -> StatusLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "StatusLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? StatusLDBModel
        } catch {
            return nil
        }
    }
}
