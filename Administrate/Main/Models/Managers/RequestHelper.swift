//
//  RequestHelper.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation

enum requestHelper{
    static func getCategories(token:String, completion:@escaping([CategoryModel]?) -> Void, completionError:@escaping(String?) -> Void){
        getListCategoriesRequest(token: token) { result in
            completion(result)
        } completionError: { error in
            completionError(error)
        }
    }
    
    static func getStatusList(token:String, completion:@escaping([StatusModel]?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        getListStatusRequest(token: token) { result in
            completion(result)
        } completionError: { error in
            completionError(error)
        }
    }
    static func getMethodsPayList(token:String, completion:@escaping([MethodsPayModel]?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        getMethodsPayRequest(token: token) { result in
            completion(result)
        } completionError: { error in
            completionError(error)
        }
    }
    
    static func getListAccount(token:String, userId:String, completion:@escaping([AccountModel]?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        
        getAccountsRequest(token: token, idUser: userId){ result in
            completion(result)
        } completionError: { err in
            completionError(err)
        }
    }
    
    static func getListTransactionByAccount(token:String, accountId:String, completion:@escaping([TransactionModelSort]?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        getListTransactionsRequest(token: token, accountId: accountId) { result in
            completion(result)
        } completionError: { err in
            completionError(err)
        }

    }
    
    static func login(data:LoginRequestModel, completion:@escaping(LoginModel?) -> Void, completionError:@escaping(String?) -> Void){
        loginRequest(data: data) { result in
            completion(result)
        } completionError: { error in
            completionError(error)
        }

    }
    
    static func addAccount(token:String, data:AddAccountRequestModel, completion:@escaping(AccountModel?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        
        AddAccountRequest(token: token, accountData: data) { result in
            completion(result)
        } completionError: { error in
            completionError(error)
        }


    }
    
    static func addTransaction(token:String, data:AddTransactionRequestModel, completion:@escaping(AddTransactionModel?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
        
        AddTransactionRequest(token: token, accountData: data) { response in
            completion(response)
        } fail: { error in
            completionError(error)
        }

    }
}
