//
//  CategoryManager.swift
//  Administrate
//
//  Created by Cristian Canedo on 09/06/22.
//

import Foundation
import CoreData
import UIKit

public class CategoryManagerLDB{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: CategoryManagerLDB = {
            let instance = CategoryManagerLDB()
            return instance
    }()
    
    private func createModel(model:CategoryModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "CategoryLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(id: model._id){
            return updateModel(entity:entity, model: model)
        }else{
            let res = CategoryLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.createAt = model.createAt
            res.data = model.data
            res.desc = model.description
            res.updateAt = model.updateAt
            res.version = NSNumber(value: model.__v)
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[CategoryModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:CategoryLDBModel, model:CategoryModel) -> Bool{
        entity.createAt = model.createAt
        entity.data = model.data
        entity.desc = model.description
        entity.updateAt = model.updateAt
        entity.version = NSNumber(value: model.__v)
        
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels() -> [CategoryModel]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CategoryLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[CategoryModel] = []
            for item in fetching {
                let it = item as! CategoryLDBModel
                let id = it.id != nil ? it.id!:""
                let description = it.desc != nil ? it.desc!:""
                let data = it.data != nil ? it.data!:""
                let created = it.createAt != nil ? it.createAt!:""
                let updated = it.updateAt != nil ? it.updateAt!:""
                let ver = Double(exactly: it.version ?? 0.0)!
                
                arrayResult.append(CategoryModel(_id: id, description: description, data: data, createAt: created, updateAt: updated, __v: ver))
            }
            return arrayResult
        }catch{
            return []
        }
    }
    
    func getModelsbyId(id:String) -> CategoryModel?{
        guard let it = ifExist(id: id) else {return nil}
        let id = it.id != nil ? it.id!:""
        let description = it.desc != nil ? it.desc!:""
        let data = it.data != nil ? it.data!:""
        let created = it.createAt != nil ? it.createAt!:""
        let updated = it.updateAt != nil ? it.updateAt!:""
        let ver = Double(exactly: it.version ?? 0.0)!
        return CategoryModel(_id: id, description: description, data: data, createAt: created, updateAt: updated, __v: ver)
    }
    
    private func ifExist(id:String) -> CategoryLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CategoryLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? CategoryLDBModel
        } catch {
            return nil
        }
    }
}
