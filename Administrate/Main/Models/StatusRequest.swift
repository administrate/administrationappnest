//
//  StatusRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation
import Alamofire

func getListStatusRequest(token:String, completion: @escaping ([StatusModel]?) -> Void, completionError: @escaping (ErrorModel?) -> Void){
    var configuration = Configuration()

    let URL = configuration.enviroment.base_url + "/status/"
    //let params : Parameters = ["user":user,"password":password]
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(URL, method: .get, encoding: JSONEncoding.default, headers: headders).responseJSON { response in
        do {
            switch response.result {
               case .success(let JSON):
                
                let responseServer = try JSONDecoder().decode(BaseArrayModel<StatusModel>.self, from: JSONSerialization.data(withJSONObject: JSON))
        
                if(responseServer.respuesta){
                    completion(responseServer.data)
                }else{
              
                    completionError(ErrorModel(message: "Error: \(response.error!)", statusCode: response.response?.statusCode ?? 0))
            
                }
               case .failure(let error):
                completionError(ErrorModel(message: "Error de repsueta: \(error)", statusCode: error.responseCode ?? 0))
            }
            
        } catch let error as NSError {
            completionError(ErrorModel(message: "Error al trnsformar datos: \(error)", statusCode: response.response?.statusCode ?? 0))
        }
    }
}
