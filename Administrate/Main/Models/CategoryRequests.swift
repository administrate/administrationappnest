//
//  CategoryRequests.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 30.0

func getListCategoriesRequest(token:String, completion: @escaping ([CategoryModel]?) -> Void, completionError: @escaping (String) -> Void){
    var configuration = Configuration()
    let URL = configuration.enviroment.base_url + "/category/"
    //let params : Parameters = ["user":user,"password":password]
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(URL, method: .get,headers: headders){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseArrayModel<CategoryModel>.self) { response in
        switch response.result {
        case .success:
            if let res = response.value{
                completion(res.data)
            }
        case let .failure(error):
            let errorRes = ErrorModel(message: response.data?.getResponseMessage ?? "", statusCode: error.responseCode ?? 0).messageByCode
            completionError(errorRes)
        }
    }
}
