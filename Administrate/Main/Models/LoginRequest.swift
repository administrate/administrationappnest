//
//  LoginRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 5.0

func loginRequest(data:LoginRequestModel, completion:@escaping(LoginModel?) -> Void, completionError:@escaping(String?) -> Void){
    var configuration = Configuration()

    let URL = configuration.enviroment.base_url + "/auth/login"
    
    AF.request(URL, method: .post, parameters: data, encoder: JSONParameterEncoder.default){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseObjectModel<LoginModel>.self) { response in
        switch response.result {
            case .success:
                if let data = response.value {
                    completion(data.data)
                }
            case let .failure(error):
                _ = response.response?.statusCode
                completionError(error.errorDescription)
        }
    }
}
