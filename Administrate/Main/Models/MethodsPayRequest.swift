//
//  MethodsPayRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 20/01/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 5.0

func getMethodsPayRequest(token:String, completion:@escaping([MethodsPayModel]?) -> Void, completionError:@escaping(ErrorModel?) -> Void){
    var configuration = Configuration()

    let URL = configuration.enviroment.base_url + "/methods-pay/"
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(URL, method: .get, headers: headders){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseArrayModel<MethodsPayModel>.self) { response in
        switch response.result {
            case .success:
                if let data = response.value {
                    completion(data.data)
                }
            case let .failure(error):
            
                _ = response.response?.statusCode
            completionError(ErrorModel(message: error.localizedDescription, statusCode: error.responseCode ?? 0))
        }
    }
    
}
