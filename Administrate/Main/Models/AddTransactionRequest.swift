//
//  AddTransactionRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 26/01/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 5.0


func AddTransactionRequest(token:String, accountData:AddTransactionRequestModel, completion:@escaping(_ response:AddTransactionModel?) -> Void, fail:@escaping(_ error:ErrorModel) -> Void){
    var configuration = Configuration()
    let url = "\(configuration.enviroment.base_url)/registers/"
    let headders:HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Bearer \(token)"]
    
    AF.request(url, method: .post, parameters: accountData, encoder: JSONParameterEncoder.default, headers: headders){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
    .responseDecodable(of: BaseObjectModel<AddTransactionModel>.self){ response in
        switch response.result {
            case .success:
                if let data = response.value {
                    completion(data.data)
                }
            case let .failure(error):
            
                _ = response.response?.statusCode
            fail(ErrorModel(message: error.localizedDescription, statusCode: error.responseCode ?? 0))
        }
    }
    
}
