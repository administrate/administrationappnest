//
//  JojoAlert.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation
import UIKit
import Lottie

class JojoAlert{
    
    struct Constants {
        static let backgroundAlphaTo:CGFloat = 0.6
    }
    
    private let backgroundView:UIView = {
        let background = UIView()
        background.backgroundColor = .secondarySystemBackground
        background.alpha = 0.0
        return background
    }()
    
    private let alertView:UIView = {
        let alert = UIView()
        alert.backgroundColor = .systemBackground
        alert.layer.masksToBounds = true
        return alert
    }()

    
    func dismissAlert(){

        alertView.alpha = 0
        for view in alertView.subviews{
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    @objc func actionCancelable(){
        self.dismissAlert()
    }
    @objc func dissmisBottomSheet(){
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.frame.origin = CGPoint(x: 0, y: self.backgroundView.frame.height)
        }, completion: {_ in
            self.backgroundView.frame.origin = CGPoint(x: 0, y: self.backgroundView.frame.height)
            self.backgroundView.removeFromSuperview()
        })
    }
    
    func showAlertLoading(on ViewController:UIViewController){
        
        guard let targetView = ViewController.view else {
            return
        }
        
        backgroundView.frame = targetView.bounds
        alertView.alpha = 1
        
        let cancelableAction = UITapGestureRecognizer(target: self, action: #selector(self.actionCancelable))
        backgroundView.addGestureRecognizer(cancelableAction)
        backgroundView.backgroundColor = .clear
        alertView.frame = CGRect(x: CGFloat((backgroundView.frame.width / 2) - 60 ) , y: CGFloat((backgroundView.frame.height / 2) - 75), width: 100, height: 100)
        alertView.cornerRadius = 12
              
        let indicator = UIActivityIndicatorView()
        indicator.frame = alertView.bounds
        indicator.color = UIColor.link //UIColor(red: 74/255, green: 34/255, blue: 181/255, alpha: 1.0)
        indicator.style = .large
        indicator.startAnimating()
        indicator.isHidden = false
        indicator.translatesAutoresizingMaskIntoConstraints = false

        alertView.addSubview(indicator)
        
        indicator.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: alertView.centerYAnchor).isActive = true
        
        let blurEffect = UIBlurEffect(style: .dark)
        let visualEffect = UIVisualEffectView(effect: blurEffect)
        visualEffect.frame = backgroundView.bounds
        
        
        backgroundView.addSubview(visualEffect)
        backgroundView.addSubview(alertView)
        targetView.addSubview(backgroundView)
        
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }
    
    func showAlertInfo(on ViewController:UIViewController, title:String, description:String, defaultTitleButton:String, secondaryTitleButton:String, type:String,  defaultButtonAction:Selector, secondaryButtonAction:Selector?){
        
        guard let targetView = ViewController.view else {return}
        
        backgroundView.frame = targetView.bounds
        //backgroundView.frame.size.width = backgroundView.frame.size.width - 24
        //backgroundView.frame.size.height = backgroundView.frame.size.height - 109
        backgroundView.backgroundColor = .black
        alertView.alpha = 1
        alertView.frame.size = CGSize(width: (targetView.frame.size.width - 80), height: 350)
        alertView.center = backgroundView.center
        alertView.cornerRadius = 12

        let imageAlert = UIImageView(frame: CGRect(x: (alertView.frame.size.width / 2) - 50, y: 28.0, width: 100.0, height: 100.0))
        
        let lblTitle = UILabel(frame: CGRect(x:16.0, y: imageAlert.frame.origin.y + imageAlert.frame.height + 20, width: alertView.frame.width - 32, height: 27.0))
            lblTitle.text = title
            lblTitle.numberOfLines = 0
            lblTitle.textColor = .darkGray
            lblTitle.font = UIFont(name: "HelveticaNeue-Bold", size: 19.0)
            lblTitle.textAlignment = .center
            
        let subtitle = UILabel(frame: CGRect(x:16.0, y: lblTitle.frame.origin.y + lblTitle.frame.height , width: alertView.frame.width - 32, height: description.stringHeight + 10))
                               
            subtitle.numberOfLines = 0
            subtitle.font = UIFont(name: "HelveticaNeue", size: 15.0)
            subtitle.text = description
            subtitle.textColor = .darkGray
            subtitle.textAlignment = .center
            
        
        let stackvVH = UIStackView(frame: CGRect(x: 8, y: alertView.frame.height - 55, width: alertView.frame.width - 16, height: 40))
        
        let defaultButton = UIButton()
        defaultButton.addTarget(ViewController, action: defaultButtonAction, for: .touchUpInside)
        defaultButton.cornerRadius = 5
        defaultButton.setTitle(defaultTitleButton, for: .normal)
        defaultButton.backgroundColor = .systemBlue
        defaultButton.translatesAutoresizingMaskIntoConstraints = false
        defaultButton.titleLabel?.font = UIFont(name: "Helvetica", size: 14.0)!
        if secondaryButtonAction != nil{
            let secondaryButton = UIButton()
            secondaryButton.addTarget(ViewController, action: secondaryButtonAction!, for: .touchUpInside)
            secondaryButton.cornerRadius = 5
            secondaryButton.borderColor = .systemBlue
            secondaryButton.setTitleColor(.systemBlue, for: .normal)
            secondaryButton.borderWidth = 1.0
            secondaryButton.titleLabel?.font = UIFont(name: "Helvetica", size: 14.0)!
            secondaryButton.setTitle(secondaryTitleButton, for: .normal)
            secondaryButton.translatesAutoresizingMaskIntoConstraints = false
            stackvVH.addArrangedSubview(secondaryButton)
        }
        stackvVH.alignment = .fill
        stackvVH.distribution = .fillEqually
        stackvVH.spacing = 8.0
        stackvVH.addArrangedSubview(defaultButton)
            
        imageAlert.image = nil
        switch type {
        case "success":
            imageAlert.image = UIImage(systemName: "checkmark.circle.fill")
            imageAlert.tintColor = .systemGreen
        case "error":
            imageAlert.image = UIImage(systemName: "xmark.circle.fill")
            imageAlert.tintColor = .systemRed
        case "info":
            imageAlert.image = UIImage(systemName: "info.circle.fill")
            imageAlert.tintColor = .systemYellow
        default:
            imageAlert.image = UIImage(systemName: "sun.dust.fill")
            imageAlert.tintColor = .cyan
        }
            
            
            alertView.addSubview(lblTitle)
            alertView.addSubview(stackvVH)
            alertView.addSubview(subtitle)
            alertView.addSubview(imageAlert)
        
        
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }
    
    func showAlertLottieLoading(on ViewController:UIViewController){
        
        guard let targetView = ViewController.view else {
            return
        }
        
        backgroundView.frame = targetView.bounds
        alertView.alpha = 1
        
        //let cancelableAction = UITapGestureRecognizer(target: self, action: #selector(self.actionCancelable))
        //backgroundView.addGestureRecognizer(cancelableAction)
        
        alertView.frame = CGRect(x: (targetView.frame.size.width/2) - ((targetView.frame.size.width - 80)/2), y: (targetView.frame.size.height/2) - 175, width: (targetView.frame.size.width - 80), height: 350)
        alertView.cornerRadius = 12
        

        let text = UILabel(frame: CGRect(x: 16.0, y: alertView.frame.height - 50, width: alertView.frame.width-32, height: 30.0))
        text.text = "Enviando datos..."
        text.textColor = UIColor.link
        text.textAlignment = .center
        text.font = UIFont.boldSystemFont(ofSize: 19.0)
        
        var animationView:AnimationView?
        animationView = .init(name: "send_data")
        animationView!.frame = CGRect(x: 10, y: 10, width: alertView.frame.width - 20, height: alertView.frame.height - 20)
        animationView!.contentMode = .scaleAspectFill
        //animationView!.backgroundColor = UIColor.red
        animationView!.loopMode = .loop
        //animationView!.animationSpeed = 0.5
        animationView!.play()
        
        alertView.addSubview(animationView!)
        alertView.addSubview(text)
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }
    
    
    func showBottomSheet(whit tilte:String, messagge:String, textActionbutton:String, actionButton:UIButton?,  on ctx:UIViewController){
        guard let targetView = ctx.view else {
            return
        }
        
        
        let widthBackgroundView = targetView.frame.width
        let heightBackgroundView = targetView.frame.height
        
        let heightAlert = 300
        
        let originYbutton = 10
        let originYviewcoments = 12
        let heightCloseButton = 32
        
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        backgroundView.frame.size = targetView.frame.size
        backgroundView.frame.origin = CGPoint(x: 0, y: targetView.frame.height)
        
        alertView.backgroundColor = .secondarySystemBackground
        alertView.alpha = 1.0
        alertView.frame.size = CGSize(width: CGFloat(widthBackgroundView) - 16.0 , height: CGFloat(heightAlert))
        alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView - 316 )
        alertView.layer.cornerRadius = 15.0
        
        
        let button = UIButton(frame:  CGRect(x: alertView.frame.width - 48, y: CGFloat(originYbutton), width: CGFloat(heightCloseButton), height: CGFloat(heightCloseButton)))
        button.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1.0)
        button.layer.cornerRadius = 16.0
        button.setImage( UIImage(systemName: "xmark") , for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(self.dissmisBottomSheet), for: .touchUpInside)
        
        
        let title = UILabel(frame:  CGRect(x: 8, y: button.frame.origin.y  + button.frame.height + CGFloat(originYviewcoments) , width: alertView.frame.width - 16, height: 20))
        title.text = tilte
        title.textAlignment = .center
        title.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        title.textColor = .lightGray
        
        let description = UILabel(frame:  CGRect(x: 8, y: title.frame.origin.y + title.frame.height + CGFloat(originYviewcoments) , width: alertView.frame.width - 16, height: messagge.stringHeight))
        description.text = messagge
        description.textAlignment = .center
        description.font = UIFont.systemFont(ofSize: 15.0, weight: .semibold)
        description.textColor = .darkGray
        description.numberOfLines = 0
        description.sizeToFit()
        
        var buttonAction = UIButton()
        
        if(actionButton != nil){
            buttonAction = actionButton!
        }else{
            buttonAction.addTarget(self, action: #selector(self.dissmisBottomSheet), for: .touchUpInside)
        }
        
        buttonAction.frame = CGRect(x: 8, y: alertView.frame.height - 60 , width: alertView.frame.width - 16, height: 45)
        buttonAction.cornerRadius = 10
        buttonAction.backgroundColor = UIColor(red: 74/255, green: 34/255, blue: 181/255, alpha: 1.0)
        buttonAction.setTitleColor(.white, for: .normal)
        buttonAction.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .bold)
        buttonAction.clipsToBounds = true
        buttonAction.setTitle(textActionbutton, for: .normal)
        
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: alertView.frame.width - 30, y: alertView.frame.height + 41, width: 20.0, height: 20.0))
        indicator.color = .white
        indicator.isHidden = false
        
        
        alertView.addSubview(title)
        alertView.addSubview(description)
        alertView.addSubview(button)
        alertView.addSubview(buttonAction)
        buttonAction.addSubview(indicator)
        backgroundView.addSubview(alertView)
        targetView.addSubview(backgroundView)
        
        
        
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.alpha = 1.0
            self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        }, completion: {_ in
            self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        })

    }
    func showToast(message : String, font: UIFont, UIIcon:String, ctx:UIViewController) {
        guard let targetView = ctx.view else {
            return
        }
        
        let widthBackgroundView = targetView.frame.width
        let heightBackgroundView = targetView.frame.height
        
        let heightAlert = 100
    
        backgroundView.backgroundColor = .clear
        backgroundView.frame.size = targetView.frame.size
        backgroundView.frame.origin = CGPoint(x: 0, y: targetView.frame.height)
        
        alertView.backgroundColor = .clear
        alertView.frame.size = CGSize(width: CGFloat(widthBackgroundView) - 16.0 , height: CGFloat(heightAlert))
        alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView - 165 )
        alertView.layer.cornerRadius = 15.0
        
        
        
        
        let toastLabel = UILabel()
        
        toastLabel.backgroundColor = UIColor.clear
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 0
        toastLabel.layer.cornerRadius = 26;
        toastLabel.clipsToBounds  =  true
        
        
        let bodyToast = UIView()
        bodyToast.backgroundColor = UIColor.systemGray.withAlphaComponent(0.6)
        bodyToast.layer.cornerRadius = 26;
        bodyToast.clipsToBounds  =  true
        
        
        
        
        var width = message.stringWidth + 58
        let height = message.stringHeight + 36
        
        
        var sizeToast = CGSize(width: width, height: height )
        
        if(width >= alertView.frame.width ){
            width = alertView.frame.width
            sizeToast =  CGSize(width: alertView.frame.width, height: height  )
            toastLabel.frame.size = CGSize(width: alertView.frame.width - 40, height: height  )
        }else{
            sizeToast =  CGSize(width: width, height: height )
            toastLabel.frame.size = CGSize(width: width - 40, height: height )
        }
        
        
        let  posX = CGFloat(alertView.frame.width/2) - CGFloat(width/2)
        let  posY = CGFloat(alertView.frame.height/2) - CGFloat(height/2)
        
        toastLabel.frame.origin = CGPoint(x: posX + 40 , y: posY)
        bodyToast.frame.size = sizeToast
        bodyToast.frame.origin = CGPoint(x: posX  , y: posY)
        
        
        let icon = UIImageView()
        icon.image = UIImage(systemName: UIIcon)
        icon.frame = CGRect(x: posX + 8, y: (posY + height/2) - CGFloat(16), width: 32, height: 32)
        icon.contentMode = .center
        icon.layer.cornerRadius = 16
        icon.tintColor = UIColor.white
        
        alertView.addSubview(bodyToast)
        alertView.addSubview(icon)
        alertView.addSubview(toastLabel)
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
            bodyToast.alpha = 0.0
            icon.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
            bodyToast.removeFromSuperview()
            icon.removeFromSuperview()
            self.dismissAlert()
        })
        
        
    }
    
}

extension String {
    var stringWidth: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.width
    }

    var stringHeight: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.height
    }
}
