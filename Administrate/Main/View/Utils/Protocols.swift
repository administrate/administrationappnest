//
//  Protocols.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/07/22.
//

import Foundation

protocol SymbolDelegate{
    func onItemSelected(item:String)
}
