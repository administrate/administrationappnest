//
//  enumClass.swift
//  Administrate
//
//  Created by Cristian Canedo on 11/06/22.
//

import Foundation

enum MenuAction {
    case categories
    case methods
    case status
}
