//
//  utilsExtensions.swift
//  Administrate
//
//  Created by Cristian Canedo on 09/06/22.
//

import Foundation
import UIKit

extension ErrorModel{
    var messageByCode:String {
        switch(self.statusCode){
            case 401: return "No estas autorizado."
            case 0: return "Error de conexión."
            default: return self.message
        }
    }
}

extension Data{
    var getResponseMessage:String{
        let errorMessage = try? JSONDecoder().decode(ErrorResponseModel.self, from: self)
        return errorMessage?.message ?? ""
    }
}

extension UIStackView {
    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
