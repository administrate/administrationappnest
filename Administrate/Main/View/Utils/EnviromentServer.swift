//
//  EnviromentServer.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation

enum Enviroment:String {
    case Dev = "dev"
    case Qa = "qa"
    case Prod = "prod"
    
    
    var base_url:String{
        switch(self){
        case .Dev: return "http://localhost:3030"
        case .Qa: return "http://192.168.1.145:3030"
        case .Prod: return "http://monkeyvalley.tech"
        }
    }
}
