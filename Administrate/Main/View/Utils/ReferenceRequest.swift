//
//  ReferenceRequest.swift
//  Administrate
//
//  Created by Cristian Canedo on 04/06/22.
//

import Foundation

enum ReferenceRequest{
    case getCategories
    case getStatus
    case getMethodsPay
    case errorResponse
    case getAccounts
    case getTransaction
    case login
    case createTransaction
    case createAccounts
    case getAnalithycs
}
