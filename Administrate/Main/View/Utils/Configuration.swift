//
//  Configuracion.swift
//  Administrate
//
//  Created by Cristian Canedo on 02/02/22.
//

import Foundation

struct Configuration{
    lazy var enviroment:Enviroment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            
            if configuration.range(of: "Dev") != nil {
                return Enviroment.Dev
            }
            if configuration.range(of: "Qa") != nil {
                return Enviroment.Qa
            }
            if configuration.range(of: "Prod") != nil {
                return Enviroment.Prod
            }
        }
        return Enviroment.Dev
    }()
}
