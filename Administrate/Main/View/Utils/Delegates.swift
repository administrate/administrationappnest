//
//  Delegates.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import Foundation

protocol CreateAccountDelegate{
    func createSucces(response:AccountModel?)
}

protocol CreateTransactionDelegate{
    func createSuccess()
}

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

