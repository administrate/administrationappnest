//
//  JojoChart.swift
//  chartSwift
//
//  Created by Cristian Canedo on 11/07/22.
//

import Foundation
import UIKit

class JojoChart{
    
    static var themeColor:UIColor = .link
    static var isActiveYAxis:Bool = true
    static var isActiveXAxis:Bool = true
    
    private static func makeContainer(_ values:[Mark], view:UIView, type:ChartType, category:CategoryChart){
        let text = "0"
        let max = getMax(from: values)
        let originLabel = self.generateLabelView(text: text)
        let background = UIView(frame: view.frame)
        background.frame.origin.x = 0
        background.frame.origin.y = 0
        let yAxisSVwidth = category == .Money ? (max.description.stringWidthChart(ofSize: 10) + 26) :max.description.stringWidthChart(ofSize: 10)
        let xStackView = self.generateStackView(aligment: .center, .horizontal, CGSize(width: background.frame.width - yAxisSVwidth, height: 18.0), CGPoint(x: 0, y: background.frame.height - 18.0), .fillEqually)

        let yStackView = self.generateStackView(aligment: .center, .vertical, CGSize(width: yAxisSVwidth, height: background.frame.height - 18.0), CGPoint(x: background.frame.width - yAxisSVwidth, y: 0), .fillEqually)
        yStackView.alignment = .fill
        
        originLabel.frame = CGRect(x: xStackView.bounds.maxX + 3, y: yStackView.bounds.maxY - 9, width: yAxisSVwidth - 8 , height: text.stringHeight)
        drawDottedLine(start: CGPoint(x: xStackView.bounds.minX, y: xStackView.bounds.minY), end: CGPoint(x: xStackView.bounds.maxX, y: xStackView.bounds.minY), view: xStackView, dotSize: 7, dotSeparation: 0, lineWidth:  0.2)
        drawDottedLine(start: CGPoint(x: background.bounds.minX, y: background.bounds.minY), end: CGPoint(x: background.bounds.minX, y: background.bounds.maxY), view: background, dotSize: 1.3, dotSeparation: 2)
    
        self.generateXAxis(values: values, xStackView: xStackView, background: background)
        if values.isEmpty || max < 10{
            let val = values.isEmpty ? "0" : "\(max)"
            self.emptyYAxis(yStackView: yStackView, background: background, value: val)
        }else{
            self.generateYAxis(yStackView: yStackView, max: getMax(from: values) , background:background, type: type, category: category)
            background.addSubview(originLabel)
        }
        
        background.addSubview(xStackView)
        background.addSubview(yStackView)
        view.addSubview(background)
    }
    
    
    private static func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView, dotSize:NSNumber, dotSeparation:NSNumber, lineWidth:CGFloat = 1, color:UIColor = UIColor.lightGray) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineDashPattern = [dotSize, dotSeparation]

        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    private static func getMax(from array:[Mark]) -> Double{
        var max = 0.0
        array.forEach { it in
            if it.value > max{
                max = it.value
            }
        }
        return max
    }
    private static func generateLabelView(text labelTxt:String) -> UILabel{
        let label = UILabel()
        label.text = labelTxt
        label.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        label.textColor = .systemGray
        label.numberOfLines = 0
        
        return label
    }
    private static func generateStackView(aligment alig:UIStackView.Alignment,_ axis:NSLayoutConstraint.Axis,_ size: CGSize,_ origin:CGPoint,_ distribution:UIStackView.Distribution) -> UIStackView{
        let stackView = UIStackView()
        stackView.alignment = alig
        stackView.axis = axis
        stackView.frame.size = size
        stackView.frame.origin = origin
        stackView.distribution = distribution
        
        return stackView
    }
    private static func generateYAxis(yStackView:UIStackView, max:Double, background:UIView, type:ChartType = .Bar, category:CategoryChart){
        let yItems = yStackView.frame.height / 5
        for i in 0...5{
            if i < 5{
                let mcd:Double = (5.0/(Double(i)-5.0) * -1.0)
                let mcdConditional = mcd > 0 ? mcd: 1
                let text = max/mcdConditional
                let parsetest = parseType(type: category, value: text)
                let label = generateLabelView(text: parsetest)
                label.frame.size = CGSize(width: yStackView.frame.width - 8, height:text.description.stringHeight)
                label.frame.origin.y = label.frame.origin.y - (text.description.stringHeight / 2)
                label.frame.origin.x = 4
                label.numberOfLines = 1
                label.minimumScaleFactor = 2
                label.adjustsFontSizeToFitWidth = true
                
                let view = UIView()
                view.frame = label.frame
                view.sizeToFit()
                view.addSubview(label)
                yStackView.addArrangedSubview(view)
                let yPosition = (yItems * CGFloat(i))
                if isActiveXAxis {
                drawDottedLine(start: CGPoint(x: background.bounds.minX, y: yPosition), end: CGPoint(x: background.bounds.maxX - yStackView.frame.width, y: yPosition), view: background, dotSize: 7, dotSeparation: 0, lineWidth: 0.2)
                }
            }
        }
    }
    private static func emptyYAxis(yStackView:UIStackView, background:UIView, value:String = "0"){
        let yItems = yStackView.frame.height / 5
        for i in 0...5{
            if i < 5{
                let text = i == 0 ? value : ""
                let label = generateLabelView(text: "\(text)")
                
                label.frame.size = CGSize(width: yStackView.frame.width, height:text.description.stringHeight)
                label.textAlignment = .left
                label.frame.origin.y = label.frame.origin.y - (text.description.stringHeight / 2)
                label.frame.origin.x = 4
                
                let view = UIView()
                view.frame = label.frame
                view.sizeToFit()
                view.addSubview(label)
                yStackView.addArrangedSubview(view)
                let yPosition = (yItems * CGFloat(i))
                if isActiveXAxis {
                drawDottedLine(start: CGPoint(x:  background.bounds.minX, y: yPosition), end: CGPoint(x: background.bounds.maxX - yStackView.frame.width, y: yPosition), view: background, dotSize: 7, dotSeparation: 0, lineWidth: 0.2)
                }
            }
        }
    }
    private static func generateXAxis(values:[Mark], xStackView:UIStackView, background:UIView){
        let xItems = xStackView.frame.width / CGFloat(values.count)
        values.enumerated().forEach{i, it in
            let label = generateLabelView(text: " \(it.name)")
            xStackView.addArrangedSubview(label)
            let xPosition = (xItems * CGFloat(i + 1))
            if isActiveYAxis{
            drawDottedLine(start: CGPoint(x: xPosition, y: background.bounds.minY), end: CGPoint(x: xPosition, y: background.bounds.maxY), view: background, dotSize: 1.3, dotSeparation: 2)
            }
        }
    }
    private static func generateSpace(_ width:CGFloat, persent:Int) -> CGFloat{
        let space = ((CGFloat(persent)*width)/100)
        return space
    }
    private static func generateBar(value:Double,position:Int, count:Int, background:UIView, max:Double, category:CategoryChart) -> UIView{
        let bar = UIView()
        let labelTotal = UILabel()
        let yAxisSVwidth = category == .Money ? (max.description.stringWidthChart(ofSize: 10) + 26) :max.description.stringWidthChart(ofSize: 10)
        let barContainer = (background.frame.width - yAxisSVwidth)/CGFloat(count)
        let space = generateSpace(barContainer, persent: 20)
        let barWidth = barContainer - space
        let barHeigth = generatePercent(value: value, background: background, max: max)
        let xOrigin = CGFloat(CGFloat(position) * barContainer + (space/2))
        let yOrigin = CGFloat(background.frame.height - barHeigth - 18.0)
        bar.backgroundColor = themeColor
        bar.layer.cornerRadius = barWidth/2 >= 6 ? 6 : barWidth/2
        bar.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        bar.frame.origin = CGPoint(x: xOrigin, y: yOrigin)
        bar.frame.size = CGSize(width: barWidth, height: barHeigth)
        let tap = MyTapGesture(target: self, action: #selector(self.actionBar(_:)))
        tap.value = value
        bar.addGestureRecognizer(tap)
        
        labelTotal.text = parseType(type: category, value: value)
        labelTotal.frame = CGRect(x: 0, y: bar.bounds.maxY - 20, width: bar.frame.width, height: 19.0)
        labelTotal.textAlignment = .center
        labelTotal.textColor = barHeigth < 15 ? .label : .white
        labelTotal.font = UIFont.systemFont(ofSize: 10.0)
        bar.addSubview(labelTotal)
        return bar
    }
    
    private static func generatePoint(value:Double,position:Int, count:Int, background:UIView, max:Double, lastValue:Double) -> UIView{
        let point = UIView()
                
        let barContainer = (background.frame.width - 30.0)/CGFloat(count)
        let positionOnView = generatePercent(value: value, background:background, max:max)
        
        let pointsize = 12
        let xOrigin = CGFloat(((Int(barContainer) * (position+1)) - (Int(barContainer) / 2))-6)
        let yOrigin = (background.frame.height - 18.0) - positionOnView - 6
        
        point.layer.cornerRadius = 6
        point.backgroundColor = .systemGreen
        point.center = CGPoint(x: xOrigin, y: yOrigin)
        point.frame.size = CGSize(width: pointsize, height: pointsize)
        
        
        return point
    }
    
    private static func generateLine(value:Double,position:Int, count:Int, background:UIView, max:Double, lastValue:Double){
        let point = UIView()
        let lpoint = UIView()
        
        let barContainer = (background.frame.width - 30.0)/CGFloat(count)
        let positionOnView = generatePercent(value: value, background:background, max:max)
        
        let pointsize = 12
        let xOrigin = CGFloat(((Int(barContainer) * (position+1)) - (Int(barContainer) / 2))-6)
        let yOrigin = (background.frame.height - 18.0) - positionOnView - 6
        
        let positionOnViewLast = generatePercent(value: lastValue, background:background, max:max)
        let xOriginLast = position == 0 ? 0.0 : CGFloat(((Int(barContainer) * (position)) - (Int(barContainer) / 2))-6)
        let yOriginLast = position == 0 ? (background.frame.height - 18.0) - 6 : (background.frame.height - 18.0) - positionOnViewLast - 6
        
        point.layer.cornerRadius = 6
        point.backgroundColor = .link
        point.center = CGPoint(x: xOrigin, y: yOrigin)
        point.frame.size = CGSize(width: pointsize, height: pointsize)
        
        lpoint.layer.cornerRadius = 6
        lpoint.backgroundColor = .systemGreen
        lpoint.center = CGPoint(x: xOriginLast, y: yOriginLast)
        lpoint.frame.size = CGSize(width: pointsize, height: pointsize)
        
        drawDottedLine(start: lpoint.center, end: point.center, view: background, dotSize: 1, dotSeparation: 0, lineWidth: 2, color: .link)
    }
    
    private static func generateLastLine(value:Double,position:Int, count:Int, background:UIView, max:Double){
        let barContainer = (background.frame.width - 30.0)/CGFloat(count)
        let positionOnView = generatePercent(value: value, background:background, max:max)
        
        let pointsize = 1.0
        let xOrigin = CGFloat(((Int(barContainer) * (position+1)) - (Int(barContainer) / 2))-6)
        let yOrigin = (background.frame.height - 18.0) - positionOnView - 6
        
        let point = UIView(frame: CGRect(x: xOrigin, y: yOrigin, width: pointsize, height: pointsize))
        let lpoint = UIView(frame: CGRect(x: background.bounds.maxX - 30.0, y: background.bounds.maxY - 18.0, width: pointsize, height: pointsize))
        
        drawDottedLine(start: lpoint.center, end: point.center, view: background, dotSize: 1, dotSeparation: 0, lineWidth: 2, color: .link)
    }
    
    private static func generatePercent(value:Double, background:UIView, max:Double)->Double{
        let viewTall = background.frame.height - 18.0
        let val = ((value*100)/max)
        let calcualate = ((val*viewTall)/100)
        return calcualate
    }
    private static func clearView(view:UIView){
        for view in view.subviews{
            view.removeFromSuperview()
        }
    }
    private static func parseType(type:CategoryChart, value:Double)-> String{
        switch type {
        case .Money:
            return formatDoubleToMoney(value: value)
        case .Points:
            return "\(Int(value))"
        }
    }
    @objc private static func actionBar(_ sender:MyTapGesture){
        print(sender.value)
    }
    
    static func barChart(values:[Mark], view:UIView, context:UIViewController, type:CategoryChart = .Points){
        let max = getMax(from: values)
        clearView(view: view)
        makeContainer(values, view:view, type: .Bar, category: type)
        values.enumerated().forEach{ i, it in
            view.addSubview(generateBar(value: it.value,position:i, count: values.count, background: view, max: max, category: type))
        }
    }
    static func lineChart(values:[Mark], view:UIView, context:UIViewController){
        let max = getMax(from: values)
        clearView(view: view)
        makeContainer(values, view: view, type: .Line, category: .Points)
        values.enumerated().forEach{ i, it in
            let lastIndex = i > 0 ? (i - 1) : 0
            generateLine(value: it.value,position:i, count: values.count, background: view, max: max, lastValue: values[lastIndex].value)
        }
        if !values.isEmpty{
        generateLastLine(value: values.last?.value ?? 0.0, position: values.count - 1, count: values.count, background: view, max: max)
        }
        values.enumerated().forEach{ i, it in
            let lastIndex = i > 0 ? (i - 1) : 0
            view.addSubview(generatePoint(value: it.value,position:i, count: values.count, background: view, max: max, lastValue: values[lastIndex].value))
        }
       
    }
}
struct Mark:Codable{
    var name:String
    var value:Double
}

enum ChartType{
    case Bar
    case Line
}
enum CategoryChart{
    case Money
    case Points
}
extension String {
    func stringWidthChart(ofSize fontSize:CGFloat) -> CGFloat{
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)], context: nil)
        return boundingBox.width
    }
}

class MyTapGesture: UITapGestureRecognizer {
    var value = Double()
    var bar = UIView()
}
