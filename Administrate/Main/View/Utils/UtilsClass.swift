//
//  UtilsClass.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation
import UIKit
//import RealmSwift
import ObjectMapper

func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,64}"

    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

func formatDoubleToMoney(value:Double)-> String{
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current

    let priceString = currencyFormatter.string(from: NSNumber(value: value))!
    return priceString
}
func formatMoneyToDouble(value:String)-> Double{
    if(value != ""){
    var clearDouble = value.replacingOccurrences(of: "$", with: "")
    clearDouble = clearDouble.replacingOccurrences(of: ",", with: "")
    return Double(clearDouble) ?? 0
    }else{
        return 0
    }
}
func formatGMTToDate(data:String)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "EEE MMM dd yyyy HH:mm:ss Z"
    guard let date = dateFormatter.date(from: data) else{ return ""}
    let calendar = Calendar.current
    let day = formarMonthOrDay(calendar.component(.day, from: date))
    let month = formarMonthOrDay(calendar.component(.month, from: date))
    
    return "\(day)/\(month)"
}
func formatDateToYMD(date:String, format:String)-> Date?{
    let dataFormat = DateFormatter()
    dataFormat.dateFormat = format
    guard let date = dataFormat.date(from: date) else{return nil}
    return date
}
func addToolbar(on ctx:UIViewController, action:Selector, titleButton:String) -> UIView {
        let barButton = UIBarButtonItem(title: titleButton, style: .plain, target: ctx, action: action)
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexible, barButton], animated: true)
        return toolBar
}
func jsonEncoder<T:Codable>(data:T) -> Data?{
    do {
        let encoder = JSONEncoder()
        let data = try encoder.encode(data)
        return data
    } catch {
        print("Unable to Encode Note (\(error))")
        return nil
    }
}

func jsonDecoder<T:Codable>(data:Data?, for type: T.Type) -> T?{
    do {
        guard let dta = data else{return nil}
        let decoder = JSONDecoder()
        let note = try decoder.decode(type, from: dta)
        return note
    } catch {
        print("Unable to Decode Note (\(error))")
        return nil
    }
}

func arrayDateSort(_ with:[TransactionModel], order:ComparisonResult)-> [TransactionModel]{
    
    let ready = with.sorted { t1, t2 in
        
        let dataFormatT1 = DateFormatter()
        let splitDateT1 = t1.createAt?.components(separatedBy: "T")
        dataFormatT1.dateFormat = "yyyy-MM-dd"// yyyy-MM-dd"
        guard let dateT1 = dataFormatT1.date(from: splitDateT1?[0] ?? "") else{return false}
        
        let dataFormatT2 = DateFormatter()
        let splitDateT2 = t2.createAt?.components(separatedBy: "T")
        dataFormatT2.dateFormat = "yyyy-MM-dd"// yyyy-MM-dd"
        guard let dateT2 = dataFormatT2.date(from: splitDateT2?[0] ?? "") else{return false}
        
        return dateT1.compare(dateT2) == order
    }
    
    
    return ready
}

func groupByDate(_ with:[TransactionModel])-> [DateComponents?:[TransactionModel]]{
    let groupDic = Dictionary(grouping: with) { (it) -> DateComponents? in
        let dataFormatT1 = DateFormatter()
        let splitDateT1 = it.createAt?.components(separatedBy: "T")
        dataFormatT1.dateFormat = "yyyy-MM-dd"// yyyy-MM-dd"
        if let dateT1 = dataFormatT1.date(from: splitDateT1?[0] ?? "") {
        let date = Calendar.current.dateComponents([.day, .year, .month], from: (dateT1))
            return date
        }else{
            return nil
        }
    }
    return groupDic
}
func formarMonthOrDay(_ item:Int) -> String{
    var strDay = "\(item)"
    if item < 10{
        strDay = "0\(item)"
    }
    return strDay
}
func formatDateComponentsString(_ item:DateComponents)-> String{
    guard let day = item.day, let month = item.month, let year = item.year else{
        return ""
    }
    var strDay = "\(day)"
    var strMonth = "\(month)"
    
    if day < 10{
        strDay = "0\(day)"
    }
    if month < 10{
        strMonth = "0\(month)"
    }
    return "\(strDay)-\(strMonth)-\(year)"
}
