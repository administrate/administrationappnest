//
//  ProfileRowTableViewCell.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import UIKit

class ProfileRowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleRow:UILabel!
    @IBOutlet weak var switchDemo: UISwitch!
    @IBOutlet weak var iconChevron: UIImageView!
    
}
