//
//  TransactionTableViewCell.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblPriceAndQuantity:UILabel!
    @IBOutlet weak var icon:UIImageView!
}
