//
//  CategoryRealmModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation
import CoreData


@objc(CategoryLDBModel)
class CategoryLDBModel: NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var desc: String?
    @NSManaged var data: String?
    @NSManaged var createAt: String?
    @NSManaged var updateAt: String?
    @NSManaged var version: NSNumber?
}
