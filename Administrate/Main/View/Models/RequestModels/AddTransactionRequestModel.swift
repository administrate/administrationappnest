//
//  AddTransactionRequestModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 25/01/22.
//

import Foundation
struct AddTransactionRequestModel:Codable{
    var idAccount:String
    var idCategory:String
    var payTypeId:String
    var typeMovement:Int?
    var idStatus:String?
    var name:String
    var price:Double
    var observations:String?
    var quantity:Int?
    var ticket:String?
    var place: String?
    var tips:Double?
}
