//
//  LoginRequestModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation

struct LoginRequestModel:Codable{
    var email:String
    var password:String
}
