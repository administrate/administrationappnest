//
//  AddAccountRequestModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import Foundation
struct AddAccountRequestModel:Codable{
    var idUser:String
    var name:String
    var balance:Double
}
