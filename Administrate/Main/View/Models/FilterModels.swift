//
//  FilterModels.swift
//  Administrate
//
//  Created by Cristian Canedo on 14/07/22.
//

import Foundation

struct FiltersModel:Codable{
    var name:String
    var value:Int
    var query:String
}
