//
//  ChartModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/06/22.
//

import Foundation
struct ChartModel:Codable{
    var labels:[String]
    var values:[Double]
}
