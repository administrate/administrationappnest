//
//  UserModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation

struct UserModel:Codable{
    var _id:String
    var email:String
    var name:String
    var phone:String
    var createAt:String
    var updateAt:String
    var status:Int
    var type:Int
    var __v:Int
}
