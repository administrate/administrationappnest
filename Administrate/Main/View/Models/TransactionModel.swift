//
//  File.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import Foundation

struct TransactionModel:Codable{
    var _id:String
    var name:String
    var price:Double
    var quantity:Int?
    var ticket:String?
    var place:String?
    var tips:Double?
    var observations:String?
    var typeMovement:Int?
    var payTypeId:String?
    var idStatus:String?
    var idAccount:String
    var idCategory:String
    var createAt:String?
    var updateAt:String?
    var categories:CategoryModel?
    var status:StatusModel?
    var methospays:MethodsPayModel?
}

struct TransactionModelSort:Codable{
    var key:String?
    var values:[TransactionModel]?
}
