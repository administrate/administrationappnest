//
//  LoginModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation

struct LoginModel:Codable{
    var accessToken:String
    var user:UserModel
}
