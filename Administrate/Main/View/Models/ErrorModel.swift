//
//  ErrorModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation

struct ErrorModel:Codable{
    var message:String
    var statusCode:Int
}
