//
//  CategoryModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation

struct CategoryModel:Codable{
    var _id:String
    var description:String
    var data:String
    var createAt:String
    var updateAt:String
    var __v:Double
}
