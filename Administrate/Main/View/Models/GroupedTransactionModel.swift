//
//  GroupedTransactionModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 07/06/22.
//

import Foundation

struct GroupedTransactionModel:Codable{
    var date:DateComponents
    var data:[TransactionModel]
}
