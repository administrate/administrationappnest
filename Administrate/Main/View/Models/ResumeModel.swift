//
//  ResumeModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/06/22.
//

import Foundation

struct ResumeModel:Codable{
    var key:String
    var values:[TransactionModel]
}

