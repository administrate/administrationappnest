//
//  MethodsPayModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import Foundation

struct MethodsPayModel:Codable{
    var _id:String
    var description:String
    var data:String
    var createAt:String
    var updateAt:String
    var __v:Double
}
