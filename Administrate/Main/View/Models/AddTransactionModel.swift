//
//  AddTransactionResponseModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 26/01/22.
//

import Foundation

struct AddTransactionModel:Codable{
    var idAccount:String
    var idCategory:String
    var payTypeId:String
    var idStatus:String?
    var name:String
    var price:Double
    var observations:String?
    var quantity:Int?
    var ticket:String?
    var place: String?
    var tips:Double?
}
