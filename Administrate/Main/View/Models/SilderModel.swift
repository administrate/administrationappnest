//
//  SilderModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 23/01/22.
//

import Foundation

struct SliderModel:Codable{
    var title:String
    var description: String
    var image:String
}
