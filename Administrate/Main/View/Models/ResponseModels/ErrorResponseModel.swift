//
//  ErrorResponseModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 09/06/22.
//

import Foundation

struct ErrorResponseModel:Codable{
    var statusCode:Int?
    var message:String?
    var error:String?
}
