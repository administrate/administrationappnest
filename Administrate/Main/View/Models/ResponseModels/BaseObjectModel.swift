//
//  BaseObjectModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation
struct BaseObjectModel<T:Codable>:Codable{
    var respuesta:Bool
    var data:T?
    var error:String?
}
