//
//  AccountModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 20/01/22.
//

import Foundation

struct AccountModel:Codable{
    var _id:String
    var balance: Double
    var idUser: String
    var name: String?
    var createAt: String
    var updateAt: String
    var __v: Int
}
