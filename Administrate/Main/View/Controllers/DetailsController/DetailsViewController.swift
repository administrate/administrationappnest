//
//  DetailsViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailsViewController: UIViewController {

    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblQuantity:UILabel!
    @IBOutlet weak var lblTips:UILabel!
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblMethodPay:UILabel!
    @IBOutlet weak var lblObservations:UILabel!
    @IBOutlet weak var tipeTransaction:UIImageView!
    @IBOutlet weak var anchorView:NSLayoutConstraint!
    
    @IBOutlet weak var viewEmpty:UIView!
    @IBOutlet weak var imageTcket:UIImageView!
    
    var detail:TransactionModel!
    private let timeoutInterval: Double = 5.0

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI(){
        let detCategory = CategoryManagerLDB.shared.getModelsbyId(id: detail.idCategory)
        let detMethods = PayMethodsManager.shared.getModelsbyId(id: detail.payTypeId ?? "")
        
        
        lblPrice.text = "$\(detail.price)"
        lblQuantity.text = "\(detail.quantity ?? 0)"
        lblTips.text = "$\(detail.tips ?? 0.0)"
        lblCategory.text = detCategory?.description ?? "Sin categoria"
        lblMethodPay.text = detMethods?.description ?? "Sin metodo de pago"
        lblObservations.text = detail.observations
        
        if(detail.typeMovement == 1){
            tipeTransaction.image = UIImage(named: "ic_down")
        }
        getImage()
    }
    func getImage(){
       
        if let url = detail.ticket{
            AF.request(url, method: .get).responseImage { response in
                                
                switch response.result {
                    case .success(let image):
                        self.viewEmpty.isHidden = true
                        self.imageTcket.isHidden = false
                        self.imageTcket.image = image
                        self.anchorView.constant = image.size.height + 20
                    
                    break
                    case let .failure(error):
                        self.viewEmpty.isHidden = false
                        self.imageTcket.isHidden = true
                    break
                  
                }
            }
        }else{
            viewEmpty.isHidden = false
            imageTcket.isHidden = true
        }
    }
}
