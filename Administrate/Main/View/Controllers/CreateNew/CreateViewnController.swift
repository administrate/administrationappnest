import UIKit
import FirebaseStorage
import Firebase
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class CreateViewnController: UIViewController {

    @IBOutlet weak var lblTitleNav:UILabel!
    @IBOutlet weak var lblTitleSecondaryNav:UILabel!
    @IBOutlet weak var navHeight:NSLayoutConstraint!
    @IBOutlet weak var formHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationBar:UIView!
    @IBOutlet weak var sv: UIScrollView!
    
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var PriceField: UITextField!
    @IBOutlet weak var quantityField: UITextField!
    @IBOutlet weak var tipsField: UITextField!
    @IBOutlet weak var placeField: UITextField!
    @IBOutlet weak var observationsField: UITextField!
    @IBOutlet weak var accountField: UITextField!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnMethodPay: UIButton!
    @IBOutlet weak var lblMethodpay: UILabel!
    @IBOutlet weak var btnstatus: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var txtExample: MDCOutlinedTextField!
    
    private let sessionUser = UserDefaults.standard
    
    private let alert = JojoAlert()
    private var token:String!
    let mViewModel = CreateTransactionViewModel()
    var imagePicker = UIImagePickerController()
    var dataPicker:Data?
    var nameImage:String?
    var typeTransaction = -1
    var callBack:CreateTransactionDelegate!
    var account:AccountModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
    }
    func initUi(){
        
        token = sessionUser.string(forKey: "_userToken")
        bindViewModel()
        mViewModel.getCategories(.getCategories)
        mViewModel.getStatus(.getStatus)
        mViewModel.getMethodsPay(.getMethodsPay)
        
        let nameTextFieldText = "Nombre(s)"
        self.txtExample.label.text = nameTextFieldText
        self.txtExample.setFloatingLabelColor(.systemGreen, for: .editing)
        self.txtExample.setFloatingLabelColor(.darkGray, for: .normal)
        self.txtExample.setNormalLabelColor(.gray, for: .normal)
        self.txtExample.delegate = self
        self.txtExample.frame.size.height = 100.0
        
    }
    func dropDownConfig(data:[Any], type:MenuAction){
        
        switch type {
        case .categories:
            btnCategory.showsMenuAsPrimaryAction = true
            var actions:[UIAction] = []
            let categories = data as? [CategoryModel] ?? []
            categories.enumerated().forEach{ i, it in
                let action = UIAction(title: it.description, handler: { res in
                    self.menuaction(it, type: type)
                })
                actions.append(action)
            }
            
            btnCategory.menu = UIMenu(title: "Selecciona una opción", options: UIMenu.Options.displayInline , children: actions)
        case .methods:
            btnMethodPay.showsMenuAsPrimaryAction = true
            var actions:[UIAction] = []
            let methods = data as? [MethodsPayModel] ?? []
            methods.enumerated().forEach{ i, it in
                let action = UIAction(title: it.description, handler: { res in
                    self.menuaction(it, type: type)
                })
                actions.append(action)
            }
            
            btnMethodPay.menu = UIMenu(title: "Selecciona una opción", options: UIMenu.Options.displayInline , children: actions)
        case .status:
            btnstatus.showsMenuAsPrimaryAction = true
            var actions:[UIAction] = []
            let methods = data as? [StatusModel] ?? []
            methods.enumerated().forEach{ i, it in
                let action = UIAction(title: it.description, handler: { res in
                    self.menuaction(it, type: type)
                })
                actions.append(action)
            }
            
            btnstatus.menu = UIMenu(title: "Selecciona una opción", options: UIMenu.Options.displayInline , children: actions)
        }
    }
    
    func menuaction(_ sender:Any, type:MenuAction){
        switch(type){
        case .categories:
            let senderCat = sender as! CategoryModel
            self.lblCategory.text = senderCat.description
            self.lblCategory.textColor = .label
            self.mViewModel.createTransactionDto.idCategory = senderCat._id
        case .methods:
            let senderCat = sender as! MethodsPayModel
            self.lblMethodpay.text = senderCat.description
            self.lblMethodpay.textColor = .label
            self.mViewModel.createTransactionDto.payTypeId = senderCat._id
        case .status:
            let senderCat = sender as! StatusModel
            self.lblStatus.text = senderCat.description
            self.lblStatus.textColor = .label
            self.mViewModel.createTransactionDto.idStatus = senderCat._id
        }
        print(self.mViewModel.createTransactionDto)
    }
    
    
    func bindViewModel(){
        mViewModel.onResultAction = {[weak self] (code:ReferenceRequest) in
            switch(code){
            case .getCategories:
                self?.onGetCategoriesEnded()
            case .getStatus:
                self?.onGetStatusEnded()
            case .getMethodsPay:
                self?.onGetMethodsPayEnded()
            case .createTransaction:
                self?.onCreateTransactionEnded()
            default:
                self?.onErrorResponse()
            }
        }
    }
    @IBAction func actionShowMore(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0) {
            let largeConfig = UIImage.SymbolConfiguration(pointSize: 17, weight: .bold, scale: .small)
            
            if(self.formHeight.constant == 260){
                self.formHeight.constant = 570
                sender.setTitle("Ver menos  ", for: .normal)
                sender.setImage(UIImage(systemName: "chevron.up.circle.fill", withConfiguration: largeConfig), for: .normal)
            }else{
                sender.setTitle("Ver más  ", for: .normal)
                self.formHeight.constant = 260
                sender.setImage(UIImage(systemName: "chevron.down.circle.fill", withConfiguration: largeConfig), for: .normal)
            }
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func getTicket(){
        loadTicket()
    }
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionSave(){
        self.alert.showAlertLottieLoading(on: self)
        if(dataPicker != nil){
            let buket = Storage.storage().reference()
            //let unicID = UIDevice.current.identifierForVendor!.uuidString
            let metadata = StorageMetadata()
            metadata.contentType = "image/png"
            buket.child("images/").child(self.nameImage!).putData(self.dataPicker!, metadata: metadata) { result, err in
                
                if err != nil{
                    self.saveData()
                }else{
                    buket.child("images/").child((result?.name)!).downloadURL(completion: { (url, err) in
                        if(url != nil){
                            self.mViewModel.createTransactionDto.ticket = url!.absoluteString
                            self.saveData()
                        }else{
                            self.saveData()
                        }
                    })
                }
            }
        }else{
            saveData()
        }
       
    }
    
    @objc func actionCloseAlert(){
        self.alert.dismissAlert()
    }
    @objc func actionAlert(){
        self.alert.dismissAlert()
        self.dismiss(animated: true){
            self.callBack.createSuccess()
        }
    }
    
    func saveData(){
        let name = nameField.text!
        let price = PriceField.text!
        var quantity = quantityField.text!
        let tips = tipsField.text!
        let place = placeField.text!
        let observations = observationsField.text!
        
        if(self.mViewModel.createTransactionDto.payTypeId == ""){
            let methodPayDefault = PayMethodsManager.shared.getModels()
            self.mViewModel.createTransactionDto.payTypeId = methodPayDefault[0]._id
        }
        
        if(quantity == "0" || quantity == ""){
            quantity = "1"
        }
        self.mViewModel.createTransactionDto.name = name
        self.mViewModel.createTransactionDto.price = formatMoneyToDouble(value: price)
        self.mViewModel.createTransactionDto.quantity = Int(quantity) ?? 1
        self.mViewModel.createTransactionDto.typeMovement = self.typeTransaction
        self.mViewModel.createTransactionDto.tips = Double(tips) ?? 0.0
        self.mViewModel.createTransactionDto.place = place
        self.mViewModel.createTransactionDto.observations = observations
        self.mViewModel.createTransactionDto.idAccount = self.account!._id
        
        mViewModel.createTransaction( .createTransaction , token: self.token, data: self.mViewModel.createTransactionDto)
    }

    
    func onGetCategoriesEnded(){
        dropDownConfig(data: mViewModel.categoriesData, type: .categories)
    }
    
    func onGetStatusEnded(){
        dropDownConfig(data: mViewModel.stautsData, type: .status)
    }
    
    func onGetMethodsPayEnded(){
        print(mViewModel.methodsPayData)
        dropDownConfig(data: mViewModel.methodsPayData, type: .methods)
    }
    func onCreateTransactionEnded(){
        self.alert.dismissAlert()
        self.clearform()
        alert.showAlertInfo(on: self, title: "Creado", description: "Se ha creado el registro correctamente.", defaultTitleButton: "Salir", secondaryTitleButton:"Cerrar", type: "success", defaultButtonAction:#selector(self.actionAlert) , secondaryButtonAction: #selector(self.actionCloseAlert))
    }
    func onErrorResponse(){
        self.alert.dismissAlert()
    }
    func clearform(){
        nameField.text = ""
        PriceField.text = ""
        
        quantityField.text = ""
        tipsField.text = ""
        placeField.text = ""
        observationsField.text = ""
        accountField.text = ""
        
        lblCategory.text = "Categoria"
        lblCategory.textColor = .systemGray3
        
        lblMethodpay.text = "Métodos de pago"
        lblMethodpay.textColor = .systemGray3
        
        lblStatus.text = "Estado"
        lblStatus.textColor = .systemGray3
    }
    
    func loadTicket(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true

            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func saveTicket(data:Data, name:String){
        self.dataPicker = data
        self.nameImage = name
    }
}

extension CreateViewnController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameField{
            PriceField.becomeFirstResponder()
        }else if textField == PriceField{
            PriceField.resignFirstResponder()
        }else if textField == quantityField{
            tipsField.becomeFirstResponder()
        }else if textField == tipsField{
            placeField.becomeFirstResponder()
        }else if textField == placeField{
            observationsField.becomeFirstResponder()
        }else if textField == observationsField{
            observationsField.resignFirstResponder()
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == nameField || textField == PriceField ){
            sv.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            if(textField == PriceField){
                let price = PriceField.text!
                PriceField.text = "\(formatMoneyToDouble(value: price))"
            }
        }else{
            sv.setContentOffset(CGPoint(x: 0.0, y: 245), animated: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        sv.setContentOffset(CGPoint(x: 0.0, y: 60.0), animated: true)
        if(textField == PriceField){
            let price = PriceField.text!
            let priceDob = Double(price)
            PriceField.text = formatDoubleToMoney(value: priceDob ?? 0)
        }
    }
}
extension CreateViewnController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage, let url = info[.referenceURL] as? NSURL else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        guard let imageData = image.pngData()else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        let timestamp = Date().currentTimeMillis()
        self.saveTicket(data: imageData, name:url.lastPathComponent ?? "\(timestamp)")
        //imageTicket.image = image
    }
}
