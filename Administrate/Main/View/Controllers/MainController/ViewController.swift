//
//  ViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    @IBOutlet weak var animationView:AnimationView!
    var mViewModel = ViewControllerViewModel()
    let sessionVar = UserDefaults.standard
    var token = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        initUi()
    }
    func initUi(){
        animationView.contentMode = .scaleAspectFit
        animationView.play(fromFrame: 0, toFrame: 50, loopMode: .none) { isEnded in
        
            if self.sessionVar.object(forKey: "_userToken") != nil {
                self.token = self.sessionVar.string(forKey: "_userToken") ?? ""
                self.mViewModel.getListCategories(.getCategories, token: self.token)
            }
        }
    }
    func bindViewModel(){
        mViewModel.refreshData = onClousureResponse
    }
    func onClousureResponse(_ code:ReferenceRequest){
        switch(code){
        case .getCategories:
            self.getCategoriesEnded()
        case .getStatus:
            self.getStatusEnded()
        case .getMethodsPay:
            self.getMethodsPayEnded()
        default:
            self.errorResponse()
        }
    }
 
    func errorResponse(){
        let error = mViewModel.errorResult
        //MARK: Agreagar alerta de error.
    }
    func getCategoriesEnded(){
        let data = mViewModel.dataListCategory
        CategoryManagerLDB.shared.saveModels(models: data) { response in
            print("CATEGORIES SAVED: \(response)")
        }
        mViewModel.getListStatus(.getStatus, token: token)
    }
    func getStatusEnded(){
        let data = mViewModel.dataListStatus
        StatusManagerLDB.shared.saveModels(models: data) { response in
            print("STATUS SAVED: \(response)")
        }
        mViewModel.getListMethodsPay(.getMethodsPay, token: self.token)
    }
    func getMethodsPayEnded(){
        let data = mViewModel.dataListMethodsPay
        PayMethodsManager.shared.saveModels(models: data) { response in
            print("PAY METHOD SAVED: \(response)")
        }
        self.nextStep()
    }
    func nextStep(){
        animationView.play(fromFrame: 50, toFrame: 0, loopMode: .none) { isEnded in
            self.animationView.stop()
            
            let mainTabBar = UITabBarController()
            let mainView = HomeViewController(nibName: "HomeViewController", bundle: nil)
            
            mainView.title = "Inicio"
            mainView.tabBarItem.image = UIImage(systemName: "house.circle")
            
            let profileView = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
            profileView.title = "Perfil"
            profileView.tabBarItem.image = UIImage(systemName: "person.crop.circle")
            
            let analithycsView = AnalithycsViewController(nibName: "AnalithycsViewController", bundle: nil)
            analithycsView.title = "Resumen"
            analithycsView.tabBarItem.image = UIImage(systemName: "slider.vertical.3")
            
            mainTabBar.viewControllers = [mainView, analithycsView, profileView]
            
            let rootViewController = UINavigationController(rootViewController: mainTabBar)
            rootViewController.navigationBar.prefersLargeTitles = true
            
            if let w = UIApplication.shared.keyWindow {
                w.rootViewController = rootViewController
                w.makeKeyAndVisible()
            }
        }
    }
}

