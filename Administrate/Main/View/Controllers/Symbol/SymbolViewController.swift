//
//  SymbolViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/07/22.
//

import UIKit

class SymbolViewController: UIViewController {

    @IBOutlet weak var collectionView:UICollectionView!
    
    var delegateSelected:SymbolDelegate!
    let mViewModel = SymbolViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    func setupCollectionView(){
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 32, height: 32)
        collectionView.collectionViewLayout = layout
        collectionView.register(SymbolIconCollectionViewCell.nib(), forCellWithReuseIdentifier: SymbolIconCollectionViewCell.identifier)
    }
}

extension SymbolViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mViewModel.getsymbols().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SymbolIconCollectionViewCell.identifier, for: indexPath) as! SymbolIconCollectionViewCell
        let item = mViewModel.getsymbols()[indexPath.row]
        cell.iconview.image = UIImage(systemName: item)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 32, height: 32)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true){
            self.delegateSelected.onItemSelected(item: self.mViewModel.getsymbols()[indexPath.row])
        }
    }
}
