//
//  BottomSheetNavViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/07/22.
//

import Foundation
import UIKit

class BottomSheetNavViewController:UINavigationController, UISheetPresentationControllerDelegate {
    
    var delegateSelected:SymbolDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc = SymbolViewController()
        vc.delegateSelected = self.delegateSelected
        vc.title = "Iconos"
        self.viewControllers = [vc]
        if #available(iOS 15.0, *) {
            sheetPresentationController?.delegate = self
            sheetPresentationController?.selectedDetentIdentifier = .medium
            sheetPresentationController?.prefersGrabberVisible = true
            sheetPresentationController?.detents = [.medium()]
        }
    }

}
