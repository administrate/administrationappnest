//
//  SymbolIconCollectionViewCell.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/07/22.
//

import UIKit

class SymbolIconCollectionViewCell: UICollectionViewCell {
    static let identifier = "symbolIconCollectionViewCell"
    
    @IBOutlet weak var iconview:UIImageView!
    
    static func nib() -> UINib{
        return UINib(nibName: "SymbolIconCollectionViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
