//
//  ProfileViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 17/01/22.
//

import UIKit
//import RealmSwift

class ProfileViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    
    let refreshControl = UIRefreshControl()
    var user:UserModel?
    let sessionUser = UserDefaults.standard
    var listProfile = [["Nombre: ","Correo: ", "Telefono: "], ["Estado: ", "Tipo: "], ["Cerrar sesión"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
    }
    override func viewDidAppear(_ animated: Bool) {
        setNavItemButton()
    }
    func initUi(){  
        setupTableView()
        let sessionUser = sessionUser.data(forKey: "_userData")
        user = jsonDecoder(data: sessionUser, for: UserModel.self)
    }
    
    func setupTableView() {
        refreshControl.attributedTitle = NSAttributedString(string: "Actualizar")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tvList.addSubview(refreshControl)
        
        let nib = UINib(nibName: "ProfileRowTableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: "profilerow")
    }
    
    func setNavItemButton(){
        self.navigationController!.navigationBar.topItem?.title = "Perfil"
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(systemName: "person.circle.fill"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btn1.addTarget(self, action: #selector(actionAdd(_:)), for: .touchUpInside)
        
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationController?.navigationBar.topItem?.setRightBarButton(item1, animated: true)
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem?.tintColor = .systemBlue
    }
    @objc func actionAdd(_ sender:UIBarButtonItem){
    }
    
    @objc func refresh(_ sender: AnyObject) {
    }
}

extension ProfileViewController:UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.listProfile.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listProfile[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "profilerow", for: indexPath) as! ProfileRowTableViewCell
        cell.switchDemo.isHidden = true
        cell.iconChevron.isHidden = false
        switch(indexPath.section){
        case 0:
            if(indexPath.row == 0){
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row]) \(user?.name ?? "")"
            }else if(indexPath.row == 1){
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row]) \(user?.email ?? "")"
            }else if(indexPath.row == 2){
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row]) \(user?.phone ?? "")"
            }
            break
        case 1:
            if(indexPath.row == 0){
                cell.switchDemo.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
                cell.switchDemo.isHidden = false
                cell.iconChevron.isHidden = true
    
                cell.switchDemo.isOn = self.user?.status == 1
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row])"
            }else if(indexPath.row == 1){
                var typeStr = "Usuario"
                if(user!.type == 0){
                    typeStr = "Administrador"
                }
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row]) \(typeStr)"
            }
        break
        case 2:
            if indexPath.row == 0{
                cell.titleRow.text = "\(listProfile[indexPath.section][indexPath.row])"
                cell.titleRow.textColor = .red
                cell.titleRow.textAlignment = .center
                cell.iconChevron.isHidden = true
            }
            break
        default:
            break
        }
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 20.0
        }else{
            return 5.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.section){
        case 2:           
            sessionUser.removeObject(forKey: "_userToken")
            sessionUser.synchronize()
            let rootViewController = LoginViewController()
            if let w = self.view.window {
                w.rootViewController = rootViewController
                self.view.window?.makeKeyAndVisible()
            }
            break
        default:
            break
        }
    }
}
