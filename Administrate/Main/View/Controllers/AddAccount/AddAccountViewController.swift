//
//  AddAccountViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import UIKit
//import RealmSwift

class AddAccountViewController: UIViewController {
    
    @IBOutlet weak var txtAccountName:UITextField!
    @IBOutlet weak var txtAccountBalance:UITextField!
    private let mViewModel = AddAccountViewModel()
    var delegateCreate:CreateAccountDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        bindViewModel()
    }
            
    func initUI(){
        if  let tok = mViewModel.sessionUser.string(forKey: "_userToken"){
            mViewModel.token = tok
        }
        let getUserData = mViewModel.sessionUser.data(forKey: "_userData")
        mViewModel.user = jsonDecoder(data: getUserData, for: UserModel.self)
        txtAccountBalance.inputAccessoryView = addToolbar(on: self, action: #selector(self.resetTapped), titleButton: "Cerrar")
    }
    
    func bindViewModel(){
        self.mViewModel.onResponse = {[weak self] (code:ReferenceRequest) in
            switch(code){
            case .createAccounts:
                self?.createAccountEnded()
            default:
                self?.errorCreateAccountEnded()
            }
        }
    }
    
    func createAccountEnded(){
        self.dismiss(animated: true){
            guard let delegate = self.delegateCreate, let response = self.mViewModel.createResponse else{
                return
            }
            delegate.createSucces(response: response)
        }
    }
    func errorCreateAccountEnded(){
    }
    @IBAction func sendData(){
        if(mViewModel.token != "" && mViewModel.user != nil){
            let name = txtAccountName.text!
            let mont = formatMoneyToDouble(value: txtAccountBalance.text!)
            guard let user = mViewModel.user else{return}
            
            let data = AddAccountRequestModel(idUser: user._id, name: name, balance: mont)
            self.mViewModel.postAccount(.createAccounts, token: mViewModel.token, data: data)
        }
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func resetTapped(){
        txtAccountBalance.resignFirstResponder()
    }
}

extension AddAccountViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtAccountName){
            txtAccountBalance.becomeFirstResponder()
        }else{
            txtAccountBalance.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == txtAccountBalance){
            let text = Double(textField.text!)
            textField.text = formatDoubleToMoney(value: text ?? 0.0)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == txtAccountBalance){
            let text = textField.text!
            textField.text = "\(formatMoneyToDouble(value: text))"
        }
    }

}
