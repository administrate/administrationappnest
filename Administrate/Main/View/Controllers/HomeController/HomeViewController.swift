//
//  HomeViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import UIKit
class HomeViewController: UIViewController {
    
    //Conexiones de interfáz
    @IBOutlet weak var tvList:UITableView!
    @IBOutlet weak var accountFirstLetter:UILabel!
    @IBOutlet weak var accountName:UILabel!
    @IBOutlet weak var accountBalance:UILabel!
    @IBOutlet weak var accountPagerControl:UIPageControl!
    @IBOutlet weak var accountPagerView:UIView!
    @IBOutlet weak var accountEmptyView:UIView!
    @IBOutlet weak var btn:UIButton!
    
    let mViewModel = HomeViewModel()
    let sessionUser = UserDefaults.standard
    let refreshControl = UIRefreshControl()
    
    //Funciones de ciclo de vida de la app
    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        setNavItemBbuttom()
    }
    
    //funciones de inicializacion
    func initUi(){
        mViewModel.token = sessionUser.string(forKey: "_userToken") ?? ""
        let getUserData = sessionUser.data(forKey: "_userData")
        mViewModel.user = jsonDecoder(data: getUserData, for: UserModel.self)
        setupTableView()
        bindViewModel()
        getData()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            
        leftSwipe.direction = .left
        rightSwipe.direction = .right

        accountPagerView.addGestureRecognizer(leftSwipe)
        accountPagerView.addGestureRecognizer(rightSwipe)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Actualizar")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tvList.addSubview(refreshControl)
    }

    
    func setNavItemBbuttom(){
        self.navigationController!.navigationBar.topItem?.title = "Registros"
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "ic_add_Account"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btn1.addTarget(self, action: #selector(actionAdd(_:)), for: .touchUpInside)
        
        let item1 = UIBarButtonItem(customView: btn1)
        item1.tintColor = .systemBlue
        self.navigationController?.navigationBar.topItem?.setRightBarButton(item1, animated: true)
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem?.tintColor = .systemBlue
    }
    func setupTableView() {
        let nib = UINib(nibName: "TransactionTableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: "cellTransaction")
    }
    func bindViewModel(){
        mViewModel.onDataResponse = onClousureResponse
    }
    
    //funciones de acciones
    
    @IBAction func actionAddTransaction(_ sender:UIButton){
        let addView = CreateViewnController(nibName: "CreateViewnController", bundle: nil)
        addView.typeTransaction = sender.tag
        addView.isModalInPresentation = true
        addView.callBack = self
        addView.account = mViewModel.dataListAccounts[accountPagerControl.currentPage]
        self.present(addView, animated: true, completion: nil)
    }
    
    @IBAction func actionAddAccount(_ sender:UIButton){
        goToAddAccount()
    }
    @objc func actionAdd(_ sender:UIBarButtonItem){
        goToAddAccount()
    }
    @objc func refresh(_ sender: AnyObject) {
        getData()
    }
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer)
    {
        switch(sender.direction){
        case .left:
            if accountPagerControl.currentPage < mViewModel.dataListAccounts.count - 1{
                accountPagerControl.currentPage += 1
            }
            break
        case .right:
            if accountPagerControl.currentPage >  0{
                accountPagerControl.currentPage -= 1
            }
            break
        default:
            break
        }
        getAccountsEnded()
    }
    //funciones de logicas
    func getData(){
        guard let dataUser = self.mViewModel.user else{return}
        mViewModel.getListAccounts( .getAccounts , token: self.mViewModel.token, userID: dataUser._id)
    }
    func goToAddAccount(){
        let addView = AddAccountViewController(nibName: "AddAccountViewController", bundle: nil)
        addView.delegateCreate = self
        self.present(addView, animated: true, completion: nil)
    }
    private func onClousureResponse(code:ReferenceRequest){
        switch(code){
        case .getAccounts:
            self.getAccountsEnded()
        case .getTransaction:
            self.getTransactionsEnded()
        default:
            self.onErrorResponse()
        }
        self.tvList.reloadData()
    }
    func getAccountsEnded(){
        if mViewModel.dataListAccounts.count > 0{
            
            guard let parse = jsonEncoder(data: mViewModel.dataListAccounts) else{return}
            sessionUser.set(parse, forKey: "_accountsData")
            self.accountEmptyView.isHidden = true
            self.accountPagerView.isHidden = false
            accountPagerControl.numberOfPages = mViewModel.dataListAccounts.count
            let selectedtAccount = mViewModel.dataListAccounts[accountPagerControl.currentPage]
            let name = selectedtAccount.name ?? "?"
            let index = name.index(name.startIndex, offsetBy: 0)
            
            self.accountFirstLetter.text =  String(name[index]).uppercased()
            self.accountName.text = selectedtAccount.name
            self.accountBalance.text = formatDoubleToMoney(value: selectedtAccount.balance)
            
            self.mViewModel.getListTransactions(.getTransaction, token: self.mViewModel.token, accountId: selectedtAccount._id)
        }else{
            self.accountEmptyView.isHidden = false
            self.accountPagerView.isHidden = true
        }
    }
    func errorResponse(){
        let error = mViewModel.errorResult
        //MARK: Agreagar alerta de error.
    }
    func getTransactionsEnded(){
        self.refreshControl.endRefreshing()
        self.tvList.reloadData()
    }
    func onErrorResponse(){
        self.refreshControl.endRefreshing()
        let error = mViewModel.errorResult
    }

}
//Implementacion de tableview
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return mViewModel.dataListTransactions.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.dataListTransactions[section].values?.count ?? 0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dataStr = mViewModel.dataListTransactions[section].key ?? ""
        let dataKey = dataStr.components(separatedBy: "GMT")
        return dataKey[0]
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)
        header.textLabel?.textAlignment = .right
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTransaction", for: indexPath) as! TransactionTableViewCell
        let item = mViewModel.dataListTransactions[indexPath.section].values?[indexPath.row]
        
        cell.lblTitle.text = item?.name ?? ""
        cell.lblPriceAndQuantity.text = "$\(item?.price  ?? 0.0) x\(item?.quantity ?? 1)"
        if(item?.typeMovement == 2){
            cell.icon.image = UIImage(named: "ic_up")
        }else{
            cell.icon.image = UIImage(named: "ic_down")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ncontroller = DetailsViewController()
        let data = mViewModel.dataListTransactions
        let item = data[indexPath.section].values?[indexPath.row]
        ncontroller.title = item?.name
        ncontroller.detail = item
        self.navigationController?.pushViewController(ncontroller, animated: true)
    }
}
extension HomeViewController: CreateAccountDelegate{
    func createSucces(response: AccountModel?) {
        self.getData()
    }
}

extension HomeViewController: CreateTransactionDelegate{
    func createSuccess() {
        let selectedtAccount = mViewModel.dataListAccounts[accountPagerControl.currentPage]
        self.mViewModel.getListTransactions(.getTransaction, token: self.mViewModel.token, accountId: selectedtAccount._id)
    }
}
