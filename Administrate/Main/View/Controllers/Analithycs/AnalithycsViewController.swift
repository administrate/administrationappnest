//
//  AnalithycsViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 11/06/22.
//

import UIKit
import SnackBar_swift
import Foundation

class AnalithycsViewController: UIViewController{
    
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnFilters: UIButton!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblStatusDescription: UILabel!
    @IBOutlet weak var charView: UIView!
    @IBOutlet weak var heigthPayControllContainer: NSLayoutConstraint!
    
    let mViewModel = AnalithycsViewModel()
    let sessionUser = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.binderViewModel()
//        SnackBar.make(in: self.view, message: "Descarga completa, ¿Deseas abrir el archivo?", duration: .lengthLong).setAction(with: "Ir", action: {
//            print("retry Tapped")
//          }).show()
        initUi()
    }
    func setupChart(values: [Mark]){
        let colorChart = UIColor(named: "EnerGreen")
        JojoChart.themeColor = colorChart!
        JojoChart.barChart(values: values, view: charView, context: self, type: .Money)
    }
    override func viewDidAppear(_ animated: Bool) {
        setNavItemButton()
    }
    func initUi(){
        mViewModel.token = sessionUser.string(forKey: "_userToken") ?? ""
        let getUserData = sessionUser.data(forKey: "_userData")
        let getaccountsData = sessionUser.data(forKey: "_accountsData")
        
        mViewModel.user = jsonDecoder(data: getUserData, for: UserModel.self)
        mViewModel.accounts = jsonDecoder(data: getaccountsData, for: [AccountModel].self)!
        configMenuButton()
        configFilterButton()
    }
    
    func configMenuButton(){
        btnAccount.showsMenuAsPrimaryAction = true
        var actions:[UIAction] = []
        mViewModel.accounts.enumerated().forEach{ i, it in
            let action = UIAction(title: it.name ?? "" , handler: { res in
                self.menuaction(it)
            })
            actions.append(action)
        }
        btnAccount.menu = UIMenu(title: "Selecciona una opción", options: UIMenu.Options.displayInline , children: actions)
       getData()
    }
    func configFilterButton(){
        btnFilters.showsMenuAsPrimaryAction = true
        
        var actions:[UIAction] = []
        
        mViewModel.filters.enumerated().forEach{ i, it in
            let action = UIAction(title: it.name, handler: { res in
                self.menuFilteraction(it)
            })
            actions.append(action)
        }
        btnFilters.menu = UIMenu(title: "Selecciona una opción", options: UIMenu.Options.displayInline , children: actions)
       getData()
    }
    func getData(){
        guard let account = mViewModel.accountSelected != nil ? mViewModel.accountSelected: mViewModel.accounts[0] else{return}
        self.lblAccount.text = account.name
        self.mViewModel.getData(.getAnalithycs, token: mViewModel.token, idAccount: account._id)
    }
    func binderViewModel(){
        mViewModel.onResponse = {code in
            switch(code){
            case .getAnalithycs:
                self.onGetAnalithycs()
            default:
                self.onErrorResponse()
            }
        }
    }
    func setNavItemButton(){
        self.navigationController?.navigationBar.topItem?.title = "Resumen"
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
        
    }
    func onErrorResponse(){
        
    }
    func onGetAnalithycs(){
        guard let data = mViewModel.getAnalithycsData else{return}
//        let dataCodable = self.requestToChart(array: data)
        let dataBySpend = self.requestToSpend(array: data)
        setupChart(values: dataBySpend)
    }
    func requestToChart(array:[ResumeModel])-> [Mark]{
        var res:[Mark] = []
        array.forEach{ it in
            let categorieModel = CategoryManagerLDB.shared.getModelsbyId(id: it.key)
            let objMk = Mark(name: categorieModel?.description ?? "", value: Double(it.values.count))
            res.append(objMk)
        }
        return res
    }
    func requestToSpend(array:[ResumeModel])-> [Mark]{
        var res:[Mark] = []
        array.forEach{ it in
            let categorieModel = CategoryManagerLDB.shared.getModelsbyId(id: it.key)
            let value = it.values.map({$0.price}).reduce(0, +)
            let objMk = Mark(name: categorieModel?.description ?? "", value: value)
            res.append(objMk)
        }
        return res
    }
    func menuaction(_ sender:AccountModel){
        self.lblAccount.textColor = .white
        self.mViewModel.accountSelected = sender
        getData()
    }
    func menuFilteraction(_ sender:FiltersModel){
        print(sender.name)
    }
    @IBAction func paymentsControlAction(_ sender: UISwitch) {
        let heigth = sender.isOn ? 88.0 : 44.0
        UIView.animate(withDuration: 0.6) {
            self.heigthPayControllContainer.constant = heigth
            self.view.layoutIfNeeded()
        }
        
    }
    @IBAction func filtersAction(_ sender:UIButton){
        let navigationController = BottomSheetNavViewController()
        navigationController.delegateSelected = self
        self.present(navigationController, animated: true, completion: nil)
    }
}
extension AnalithycsViewController:SymbolDelegate{
    func onItemSelected(item: String) {
        print(item)
    }
}
