//
//  LoginViewController.swift
//  Administrate
//
//  Created by Cristian Canedo on 18/01/22.
//

import UIKit
class LoginViewController: UIViewController {

    @IBOutlet weak var constraintFormBody:NSLayoutConstraint!
    @IBOutlet weak var constraintViewMail:NSLayoutConstraint!
    @IBOutlet weak var constraintViewPassword:NSLayoutConstraint!
    @IBOutlet weak var viewTfMail:UIView!
    @IBOutlet weak var viewTfPassword:UIView!
    @IBOutlet weak var btnBack:UIButton!
    @IBOutlet weak var btnLogin:UIButton!
    @IBOutlet weak var txtMail:UITextField!
    @IBOutlet weak var txtPass:UITextField!
    
    @IBOutlet weak var imgSlider:UIImageView!
    @IBOutlet weak var lblTitleSlider:UILabel!
    @IBOutlet weak var lblDescriptionSlider:UILabel!
    @IBOutlet weak var viewBodySlider:UIView!
    @IBOutlet weak var pagerSlider:UIPageControl!
    
    private let mViewModel = LoginViewModel()
    private let userDefault = UserDefaults.standard
    private let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        configSlider()
    }
    
    func bindViewModel(){
        mViewModel.refreshData = {[weak self] (code:ReferenceRequest) in
            DispatchQueue.main.async {
                switch(code){
                case .login:
                    self?.onResponseLogin()
                default:
                    self?.onErrorLogin()
                }
            }
        }
    }
    func configSlider(){
        mViewModel.arraySlider.append(SliderModel(title: "Administra tu dinero", description: "iAdmin una app pensada para la administración de tu economía.", image: "ic_administration"))
        mViewModel.arraySlider.append(SliderModel(title: "Programa tus metas", description: "Con iAdmin podrás programar los logros que deseas realizar, asi podras administrar tu dinero y conseguir ahorrar más", image: "ic_code"))
        mViewModel.arraySlider.append(SliderModel(title: "Encuentra tus fugas", description: "Con iAdmin podrás saber en que gastas más y podrás tomar acciones al respecto", image: "ic_bug"))
        mViewModel.arraySlider.append(SliderModel(title: "Análisis de tus finanzas", description: "Con iAdmin podrás programar los logros que deseas realizar, asi podras administrar tu dinero y conseguir ahorrar más", image: "ic_finance"))
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            
        leftSwipe.direction = .left
        rightSwipe.direction = .right

        viewBodySlider.addGestureRecognizer(leftSwipe)
        viewBodySlider.addGestureRecognizer(rightSwipe)
        
    }
    
    @IBAction func btnContinue(_ sender:UIButton){
        if sender.tag == 0 {
            firstAnimation()
        }else{
            alert.showAlertLoading(on: self)
            self.login()
        }
    }
    @IBAction func actionBack(_ sender:UIButton){
        secontAnimation()
    }
    
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer)
    {
        switch(sender.direction){
        case .left:
            if pagerSlider.currentPage <         mViewModel.arraySlider.count - 1{
                pagerSlider.currentPage += 1
            }
            
            break
        case .right:
            if pagerSlider.currentPage >  0{
                pagerSlider.currentPage -= 1
            }
            break
        default:
            break
        }
      onSwiped()
    }
    func onSwiped(){
        lblTitleSlider.text =         mViewModel.arraySlider[pagerSlider.currentPage].title
        lblDescriptionSlider.text =         mViewModel.arraySlider[pagerSlider.currentPage].description
        let img = UIImage(named:         mViewModel.arraySlider[pagerSlider.currentPage].image)
        imgSlider.image = img
    }
    func onResponseLogin(){
        alert.dismissAlert()
        if let loginData = mViewModel.loginData{

            guard let userData = jsonEncoder(data: loginData.user) else{return}
            userDefault.set(userData, forKey: "_userData")
            userDefault.set(loginData.accessToken, forKey: "_userToken")
            userDefault.synchronize()
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "splashscreen") as! ViewController
            if let w = self.view.window {
                w.rootViewController = rootViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    func onErrorLogin(){
        alert.dismissAlert()
        if let error = mViewModel.errorData{
            let errorAlert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            errorAlert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
          
        }
    }
    func firstAnimation(){
        self.viewTfPassword.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0.0) {
            self.constraintViewMail.constant = -350
            self.btnBack.isHidden = false
            self.view.layoutIfNeeded()
        } completion: { isComplete in
            self.viewTfMail.isHidden = true
            self.btnLogin.setTitle("Iniciar sesión", for: .normal)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.1) {
            self.constraintViewPassword.constant = 0
            self.view.layoutIfNeeded()
        }
        btnLogin.tag = 1
        txtPass.becomeFirstResponder()
    }
    func secontAnimation(){
        self.viewTfMail.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0.1) {
            self.constraintViewMail.constant = 0
            self.btnBack.isHidden = true
            self.view.layoutIfNeeded()
        }completion: { isComplete in
            self.btnLogin.setTitle("Continuar", for: .normal)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.1) {
            self.constraintViewPassword.constant = 350
            self.view.layoutIfNeeded()
        }completion: { isComplete in
            self.viewTfPassword.isHidden = true
        }
        btnLogin.tag = 0
        txtMail.becomeFirstResponder()
    }
    func login(){
        let mail = txtMail.text!
        let password = txtPass.text!
        if(mail != "" && password != ""){
            mViewModel.login(data: LoginRequestModel(email: mail, password: password))
        }
    }

}

extension LoginViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5, delay: 0.0) {
            self.constraintFormBody.constant = self.constraintFormBody.constant + 290.0
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5, delay: 0.0) {
            self.constraintFormBody.constant = 20
            self.view.layoutIfNeeded()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMail{
            if isValidEmail(txtMail.text!){
                btnLogin.backgroundColor = .systemBlue
                btnLogin.tintColor = .white
                btnLogin.isEnabled = true
            }else{
                btnLogin.backgroundColor = .systemGray6
                btnLogin.tintColor = .lightGray
                btnLogin.isEnabled = false
            }
        }
        if(string == " "){
            return false
        }
        return true
    }
}
