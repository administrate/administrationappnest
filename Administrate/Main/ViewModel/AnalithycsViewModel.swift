//
//  AnalithycsViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/06/22.
//

import Foundation

class AnalithycsViewModel{
    
    var requestCode:ReferenceRequest!
    var onResponse = {(code:ReferenceRequest) -> () in}
    
    var getAnalithycsData:[ResumeModel]? = nil {didSet{onResponse(requestCode)}}
    var errorResponse:ErrorModel? = nil {didSet{onResponse(.errorResponse)}}
    var token = ""
    var user:UserModel?
    var accounts:[AccountModel] = []
    var filters:[FiltersModel] = [FiltersModel(name: "Último mes", value: 1, query: "")]
    var accountSelected:AccountModel?
    
    func getData(_ rc:ReferenceRequest, token:String, idAccount:String){
        self.requestCode = rc
        AnalithycsRequest(token: token, idAccount: idAccount) { response in
            self.getAnalithycsData = response
        } failure: { error in
            self.errorResponse = error
        }
    }
}
