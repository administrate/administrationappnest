//
//  AddAccountViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 22/01/22.
//

import Foundation

class AddAccountViewModel{
    
    var requestCode:ReferenceRequest!
    var onResponse = {(_ code:ReferenceRequest) -> () in}
    var createResponse:AccountModel? = nil {didSet{onResponse(requestCode)}}
    var errorResponse:ErrorModel? = nil {didSet{onResponse(.errorResponse)}}
    
    var user:UserModel?
    let sessionUser = UserDefaults.standard
    var token = ""
    
    func postAccount(_ rc:ReferenceRequest, token:String, data:AddAccountRequestModel){
        self.requestCode = rc
        requestHelper.addAccount(token: token, data: data) { response in
            guard let res = response else{
                self.errorResponse = ErrorModel(message: "Datos nulos", statusCode: -1)
                return
            }
            self.createResponse = res
        } completionError: { err in
            self.errorResponse = err
        }

    }
    
}
