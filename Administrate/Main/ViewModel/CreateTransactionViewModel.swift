//
//  CreateTransactionViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 24/01/22.
//

import Foundation

class CreateTransactionViewModel{
    
    var createTransactionDto = AddTransactionRequestModel(idAccount: "", idCategory: "", payTypeId: "", typeMovement: -1, idStatus: nil, name: "", price: 0.0, observations: nil, quantity: nil, ticket: nil, place: nil, tips: nil)
    
    var requestCode:ReferenceRequest!
    var onResultAction = {(code:ReferenceRequest) -> () in}
    var categoriesData:[CategoryModel] = []{didSet{onResultAction(requestCode)}}
    var stautsData:[StatusModel] = []{didSet{onResultAction(requestCode)}}
    var methodsPayData:[MethodsPayModel] = []{didSet{onResultAction(requestCode)}}
    var methodPayByQueryData:MethodsPayModel? = nil{didSet{onResultAction(requestCode)}}
    var createtransaction:AddTransactionModel? = nil{didSet{onResultAction(requestCode)}}
    var onErrorResultData:ErrorModel? = nil{didSet{onResultAction(.errorResponse)}}
    var arrayCategories:[CategoryModel] = []
    
    
    
    func getCategories(_ rc:ReferenceRequest){
        self.requestCode = rc
        self.categoriesData = CategoryManagerLDB.shared.getModels()
    }
    func getStatus(_ rc:ReferenceRequest){
        self.requestCode = rc
        self.stautsData = StatusManagerLDB.shared.getModels()
    }
    func getMethodsPay(_ rc:ReferenceRequest){
        self.requestCode = rc
        self.methodsPayData = PayMethodsManager.shared.getModels()
        
    }
    func createTransaction(_ rc:ReferenceRequest, token:String, data:AddTransactionRequestModel){
        self.requestCode = rc
        
        requestHelper.addTransaction(token: token, data: data) { response in
            self.createtransaction = response
        } completionError: { error in
            self.onErrorResultData = error
        }

    }

}
