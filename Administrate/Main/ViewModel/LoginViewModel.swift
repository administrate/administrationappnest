//
//  LoginViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 19/01/22.
//

import Foundation

class LoginViewModel{
    var refreshData = {(code:ReferenceRequest) -> () in}
    var loginData:LoginModel? = nil{didSet{refreshData(.login)}}
    var errorData:String? = nil{didSet{refreshData(.errorResponse)}}
    var arraySlider:[SliderModel] = []

    func login(data:LoginRequestModel){
        requestHelper.login(data: data) { result in
            self.loginData = result
        } completionError: { err in
            self.errorData = err
        }

    }
}
