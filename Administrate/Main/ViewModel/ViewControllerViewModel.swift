//
//  CategoriesViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation

class ViewControllerViewModel {
    
    var requestCode:ReferenceRequest!
    var refreshData = {(event:ReferenceRequest) -> () in}
    var onErrorResponse = {(event:Int) -> () in}
    var dataListCategory:[CategoryModel] = []{didSet{refreshData(requestCode)}}
    var dataListStatus:[StatusModel] = []{didSet{refreshData(requestCode)}}
    var dataListMethodsPay:[MethodsPayModel] = []{didSet{refreshData(requestCode)}}
    var errorResult:ErrorModel? = nil{didSet{refreshData(.errorResponse)}}
    var errorResultCat:String? = nil{didSet{refreshData(.errorResponse)}}
    
    func getListCategories(_ rc:ReferenceRequest, token:String){
        requestCode = rc
        requestHelper.getCategories(token: token) { result in
            self.dataListCategory = result ?? []
        } completionError: { err in
            self.errorResultCat = err
        }
    }
    func getListStatus(_ rc:ReferenceRequest, token:String){
        requestCode = rc
        requestHelper.getStatusList(token: token)
        { result in
            self.dataListStatus = result ?? []
        } completionError: { err in
            self.errorResult = err
        }
    }
    func getListMethodsPay(_ rc:ReferenceRequest, token:String){
        requestCode = rc
        requestHelper.getMethodsPayList(token: token) { result in
            self.dataListMethodsPay = result ?? []
        } completionError: { err in
            self.errorResult = err
        }

    }

}
