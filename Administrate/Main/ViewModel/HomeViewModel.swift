//
//  HomeViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/01/22.
//

import Foundation

class HomeViewModel{
    var requestCode:ReferenceRequest!
    var onDataResponse = {(event:ReferenceRequest) -> () in}
    var dataListTransactions:[TransactionModelSort] = []{didSet{onDataResponse(requestCode)}}
    var dataListAccounts:[AccountModel] = []{didSet{onDataResponse(requestCode)}}
    var errorResult:ErrorModel? = nil{didSet{onDataResponse(.errorResponse)}}
    var token = ""
    var user:UserModel?
    
    func getListAccounts(_ rc:ReferenceRequest, token:String, userID:String){
        self.requestCode = rc
        requestHelper.getListAccount(token: token, userId: userID) { result in
            self.dataListAccounts = result ?? []
        } completionError: { err in
            self.errorResult = err
        }
    }
    
    func getListTransactions(_ rc:ReferenceRequest, token:String, accountId:String){
        self.requestCode = rc
        requestHelper.getListTransactionByAccount(token: token, accountId: accountId) { result in
            self.dataListTransactions = result ?? []
        } completionError: { err in
            self.errorResult = err
        }
    }
}
