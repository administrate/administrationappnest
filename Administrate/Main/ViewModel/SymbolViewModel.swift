//
//  SymbolViewModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 13/07/22.
//

import Foundation

class SymbolViewModel{
    
    func getsymbols() -> [String] {
        if let bundle = Bundle(identifier: "com.apple.CoreGlyphs"),
            let resourcePath = bundle.path(forResource: "symbol_search", ofType: "plist"),
            let plist = NSDictionary(contentsOfFile: resourcePath) {
            
            var componentArray: [Any]
            componentArray = plist.allKeys
            return componentArray as! [String]
        }
        return []
    }
}
